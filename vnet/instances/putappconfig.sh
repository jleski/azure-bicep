#!/usr/bin/env bash
# putappconfig.sh
# Script to put instance configuration to key vault base64 encoded secrets.
# Updates AppConfig references to key vault secrets for keeping track of the items.
#
# Expected instance/environment file structure:
# ./<instance>.env      Instance environment variables for deploy.sh script
# ./<instance>/*.json   Instance template parameters
#
# This script stores the above configuration in Key Vault and notes a reference
# in App Configuration for future use and configuration management.
#
# Key Vault secrets are stored using unique identifier names.
# App Configuration usese combination of labels and key namespaces to distinguish
# between different instances, environments and templates. This supports sharing
# the same Key Vault and App Configuration store between multiple, generic use cases
# that utilize the same configuration file structure.
# 
# App Configuration and Key Vault configuration can be read from environment variables or
# from optional file: <instance>.config.env
#
# Usage example:
# INSTANCE=vnet-test-19 LABEL=myorg KEYVAULT=mykeyvault APPCONFIG=myappconfig ./putappconfig.sh

set -e

SUBSCRIPTION=$(az account show -o tsv --query "name")
SUBSCRIPTIONID=$(az account show -o tsv --query "id")

# use default instance unless instance variable is set
: "${INSTANCE:=dev}"

# load instance variables from instance dotenv if available
if test -f "${INSTANCE}.config.env"; then
  echo -e "> Loading instance variables from: \t./${INSTANCE}.config.env"
  set -o allexport
  source "${INSTANCE}.config.env"
  set +o allexport
fi

: "${APP:=${1}}"
: "${KEYVAULT:=${2}}"
: "${APPCONFIG:=${3}}"
: "${LABEL:=${INSTANCE}}"

# define key namespace version (used as: <key>@version/...)
: "${NS_VERSION:=27-03-2023}"

if test -z "${APP}"; then
    echo "ERROR: App name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name]"
    echo -e "\tapp name = can be set using APP instance variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT instance variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG instance variable or as command line parameter"
    exit 1
fi

if test -z "${KEYVAULT}"; then
    echo "ERROR: Key Vault name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name]"
    echo -e "\tapp name = can be set using APP instance variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT instance variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG instance variable or as command line parameter"
    exit 1
fi

if test -z "${APPCONFIG}"; then
    echo "ERROR: App Config store name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name]"
    echo -e "\tapp name = can be set using APP instance variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT instance variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG instance variable or as command line parameter"
    exit 1
fi

echo -e "Syncing instance configuration to ${KEYVAULT} vault and ${APPCONFIG} store...\n"
echo -e "  Subscription is: \t\t${SUBSCRIPTION}"
echo -e "  \t\t\t\t${SUBSCRIPTIONID}"
echo -e "  App name is: \t\t\t${APP}"
echo -e "  Instance dotenv is: \t\t${INSTANCE}.env"
echo -e "  Parameters path is: \t\t./${INSTANCE}"
echo -e "  Encoding content in: \t\tbase64"
echo -e "  AppConfig references in: \t${APPCONFIG}"
echo -e "  AppConfig label is: \t\t${LABEL}"
echo -e "  AppConfig ns version is: \t@${NS_VERSION}"

sleep 4

if test -z "${INSTANCE}"; then
    echo "ERROR: INSTANCE not set!"
    exit 1
fi

if ! test -f "./${INSTANCE}.env"; then
    echo "ERROR: ./${INSTANCE}.env not found!"
    exit 1
fi

if ! test -d "./${INSTANCE}"; then
    echo "ERROR: Path ./${INSTANCE} not found or not directory!"
    exit 1
fi

DOTENV_SECRET_NAME="${APP}-instances-${INSTANCE}-dotenv"
DOTENV_SECRET="$(echo ${DOTENV_SECRET_NAME} | sha256sum | awk '{print $1}' | base64 -w 0)"
DOTENV_SECRET="${DOTENV_SECRET%?*}"
echo -e "\n> Syncing ${INSTANCE}.env to vault ${DOTENV_SECRET}..."

az keyvault secret set \
    --vault-name "${KEYVAULT}" \
    --name "${DOTENV_SECRET}" \
    --encoding base64 \
    --file "./${INSTANCE}.env" >/dev/null

DOTENV_KEY="${APP}/instances@${NS_VERSION}/${INSTANCE}.env"
echo -e "> Updating reference in appconfig ${APPCONFIG}/${DOTENV_KEY}..."
az appconfig kv set-keyvault \
    --name "${APPCONFIG}" \
    --key "${DOTENV_KEY}" \
    --label "${LABEL}" \
    --secret-identifier "https://${KEYVAULT}.vault.azure.net/secrets/${DOTENV_SECRET}" \
    --yes >/dev/null

echo -e "\n  Scanning instance configuration in ./$INSTANCE/*.json" 
for paramfile in ./$INSTANCE/*.json; do
    if ! test -f $paramfile; then
        echo -e "# ERROR: Problem reading $paramfile, skipping..."
        continue
    fi
    file=$(basename ${paramfile})
    name=$(basename ${paramfile} .json)
    PARAM_SECRET_NAME="${APP}-instance-${INSTANCE}-param-${name}"
    PARAM_SECRET="$(echo ${PARAM_SECRET_NAME} | sha256sum | awk '{print $1}' | base64 -w 0)"
    PARAM_SECRET="${PARAM_SECRET%?*}"
    echo "> Syncing ${file} to vault ${KEYVAULT}/${PARAM_SECRET}..."
    az keyvault secret set --vault-name "${KEYVAULT}" --name "${PARAM_SECRET}" --encoding base64 -f "./${INSTANCE}/${file}" >/dev/null
    PARAM_KEY="${APP}/instances@${NS_VERSION}/${INSTANCE}/${name}.json"
    echo -e "> Updating reference in appconfig ${APPCONFIG}/${PARAM_KEY}..."
    az appconfig kv set-keyvault \
        --name "${APPCONFIG}" \
        --key "${PARAM_KEY}" \
        --label "${LABEL}" \
        --secret-identifier "https://${KEYVAULT}.vault.azure.net/secrets/${PARAM_SECRET}" \
        --yes >/dev/null

done