// ,--.   ,--.,--.  ,--.         ,--.   
//  \  `.'  / |  ,'.|  | ,---. ,-'  '-. 
//   \     /  |  |' '  || .-. :'-.  .-' 
//    \   /   |  | `   |\   --.  |  |   
//     `-'    `--'  `--' `----'  `--'   
// Virtual Network

// parameters-common.json
param tags object

// module parameters (parameters-template.json)
@description('Required. Name of the Virtual Network')
param vnetName string

@description('Required. An Array of 1 or more IP Address Prefixes for the Virtual Network.')
param addressPrefixes array

// optional parameters
@description('Optional. Location for all resources.')
param location string = resourceGroup().location

@description('Optional. An Array of subnets to deploy to the Virtual Network.')
param subnets array = []

@description('Optional. Array of role assignment objects that contain the \'roleDefinitionIdOrName\' and \'principalId\' to define RBAC role assignments on this resource. In the roleDefinitionIdOrName attribute, you can provide either the display name of the role definition, or its fully qualified ID in the following format: \'/providers/Microsoft.Authorization/roleDefinitions/c2f4ef07-c644-48eb-af81-4b1b4947fb11\'.')
param roleAssignments array = []

@description('Optional. Virtual Network Peerings configurations.')
param peerings array = []

@description('Optional. Switch to enable to create new Route Table.')
param createRouteTable bool = false

@description('Optional. Name for the optional Route Table.')
param routeTableName string = 'rt-default'

@description('Optional. Array of role assignment objects that contain the \'roleDefinitionIdOrName\' and \'principalId\' to define RBAC role assignments on this resource. In the roleDefinitionIdOrName attribute, you can provide either the display name of the role definition, or its fully qualified ID in the following format: \'/providers/Microsoft.Authorization/roleDefinitions/c2f4ef07-c644-48eb-af81-4b1b4947fb11\'.')
param routeTableRoleAssignments array = []

@description('Optional. An Array of Routes to be established within the hub route table.')
param routeTableRoutes array = []

@description('Optional. Subscription ID of the Resource Group for the Route Table.')
param routeTableSubscriptionId string = subscription().subscriptionId

@description('Optional. Resource Group name for the Route Table.')
param routeTableResourceGroup string = resourceGroup().name

module routeTables '../lib/modules/Microsoft.Network/routeTables/deploy.bicep' = if (createRouteTable == true) {
  name: 'module-routetable-${uniqueString(deployment().name, location)}'
  scope: resourceGroup(routeTableSubscriptionId, routeTableResourceGroup)
  params: {
    tags: tags
    location: location
    // Required parameters
    name: routeTableName
    roleAssignments: routeTableRoleAssignments
    routes: routeTableRoutes
    // Non-required parameters
    enableDefaultTelemetry: false
  }
}

module virtualNetworks '../lib/modules/Microsoft.Network/virtualNetworks/deploy.bicep' = {
  name: 'module-vnet-${vnetName}-${uniqueString(deployment().name, location)}'
  dependsOn: [
    routeTables
  ]
  params: {
    tags: tags
    location: location
    // Required parameters
    addressPrefixes: addressPrefixes
    name: vnetName
    // Non-required parameters
    enableDefaultTelemetry: false
    subnets: subnets
    roleAssignments: roleAssignments
    peerings: peerings
  }
}
