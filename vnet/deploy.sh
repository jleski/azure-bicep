#!/usr/bin/env bash
#      ,--.              ,--.                        ,--.      
#    ,-|  | ,---.  ,---. |  | ,---.,--. ,--.   ,---. |  ,---.  
#   ' .-. || .-. :| .-. ||  || .-. |\  '  /   (  .-' |  .-.  | 
#   \ `-' |\   --.| '-' '|  |' '-' ' \   '.--..-'  `)|  | |  | 
#    `---'  `----'|  |-' `--' `---'.-'  / '--'`----' `--' `--' 
#                 `--'             `---'                       
#
# Script to deploy Bicep template using az cli.
# Use optional parameter --validate to validate the deployment instead of creating the deployment.

# abort on errors
set -e

# use default environment unless environment variable is set
: "${ENVIRONMENT:=dev}"

echo -e "Deploying template to ${ENVIRONMENT} environment...\n"

# load environment variables from environment dotenv if available
if test -f "environments/${ENVIRONMENT}.env"; then
  echo -e "> Loading environment variables from: \t./environments/${ENVIRONMENT}.env"
  set -o allexport
  source "environments/${ENVIRONMENT}.env"
  set +o allexport
fi

SUBSCRIPTION=$(az account show -o tsv --query "name")
SUBSCRIPTIONID=$(az account show -o tsv --query "id")

# use defaults unless environment variable is set
: "${RESOURCEGROUP:=rg-demo}"
: "${PARAMOVERRIDE:=}"
: "${DEPLOYMENTMODE:=Incremental}"

# additional options to pass to az deployment group create
AZ_OPTS=""

echo -e "  Subscription is: \t\t${SUBSCRIPTION}"
echo -e "  \t\t\t\t${SUBSCRIPTIONID}"
echo -e "  Resource group is: \t\t${RESOURCEGROUP}"
echo -e "  Deployment mode is: \t\t${DEPLOYMENTMODE}"
echo -e "  Parameters are in: \t\t./environments/${ENVIRONMENT}/\n"

sleep 2

start_time="$(date '+%s')"

if test -f "${PARAMOVERRIDE}"; then
  echo -e "> Loading override parameters from: \t${PARAMOVERRIDE}"
  AZ_OPTS="--parameters ${PARAMOVERRIDE}"
fi

TASK=create
if ! test -z "${1}"; then
  if test "${1}" == "--validate"; then
    TASK=validate
  fi
fi

echo "> Proceeding to $TASK the deployment..."
az deployment group ${TASK} \
  --name "deploy-main-${ENVIRONMENT}" \
  --resource-group "${RESOURCEGROUP}"\
  --template-file template.bicep \
  --parameters "./environments/${ENVIRONMENT}/parameters-common.json" \
  --parameters "./environments/${ENVIRONMENT}/parameters-template.json" \
  --mode "${DEPLOYMENTMODE}" \
  ${AZ_OPTS}

duration=$(($(date '+%s') - start_time))
ds=$((duration % 60))
dm=$(((duration / 60) % 60))
dh=$((duration / 3600))
TZ=UTC0 printf 'Task %s stack duration: %d:%02d:%02d\n' "${TASK}" "${dh}" "${dm}" "${ds}"
