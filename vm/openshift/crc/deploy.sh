#!/usr/bin/env bash
#      ,--.              ,--.                        ,--.      
#    ,-|  | ,---.  ,---. |  | ,---.,--. ,--.   ,---. |  ,---.  
#   ' .-. || .-. :| .-. ||  || .-. |\  '  /   (  .-' |  .-.  | 
#   \ `-' |\   --.| '-' '|  |' '-' ' \   '.--..-'  `)|  | |  | 
#    `---'  `----'|  |-' `--' `---'.-'  / '--'`----' `--' `--' 
#                 `--'             `---'                       
#
# Script to deploy Bicep template using az cli.
# Use optional parameter --validate to validate the deployment instead of creating the deployment.

# abort on errors
set -e

SUBSCRIPTION=$(az account show -o tsv --query "name")
SUBSCRIPTIONID=$(az account show -o tsv --query "id")

# use defaults unless environment variable is set
: "${RESOURCEGROUP:=rg-crc-demo}"
: "${PARAMOVERRIDE:=}"
: "${DEPLOYMENTMODE:=Incremental}"

echo -e "Deploying stack to Azure...\n"

# additional options to pass to az deployment group create
AZ_OPTS=""

echo -e "  Subscription is: \t\t${SUBSCRIPTION}"
echo -e "  \t\t\t\t${SUBSCRIPTIONID}"
echo -e "  Resource group is: \t\t${RESOURCEGROUP}"
echo -e "  Deployment mode is: \t\t${DEPLOYMENTMODE}\n"

sleep 2

start_time="$(date '+%s')"

if test -f "${PARAMOVERRIDE}"; then
  echo -e "> Loading override parameters from: \t${PARAMOVERRIDE}"
  AZ_OPTS="--parameters ${PARAMOVERRIDE}"
fi

TASK=create
if ! test -z "${1}"; then
  if test "${1}" == "--validate"; then
    TASK=validate
  fi
fi

echo "> Proceeding to $TASK the deployment..."
az deployment group ${TASK} \
  --name deploy-main \
  --resource-group "${RESOURCEGROUP}"\
  --template-file template.bicep \
  --parameters parameters-common.json \
  --parameters parameters-template.json \
  --mode "${DEPLOYMENTMODE}" \
  ${AZ_OPTS}

duration=$(($(date '+%s') - start_time))
ds=$((duration % 60))
dm=$(((duration / 60) % 60))
dh=$((duration / 3600))
TZ=UTC0 printf 'Task %s stack duration: %d:%02d:%02d\n' "${TASK}" "${dh}" "${dm}" "${ds}"
