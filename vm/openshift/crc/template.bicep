                          
//  ,-----.,------.  ,-----. 
// '  .--./|  .--. ''  .--./ 
// |  |    |  '--'.'|  |     
// '  '--'\|  |\  \ '  '--'\ 
//  `-----'`--' '--' `-----' 
// Code Ready Containers (OpenShift Local)

// parameters-common.json
param tags object
param app string
param environment string

// module parameters (parameters-template.json)
param adminUserName string

// optional parameters
@description('Optional. Location for all resources.')
param location string = resourceGroup().location

@description('Optional. Enables system assigned managed identity on the resource. The system-assigned managed identity will automatically be enabled if extensionAadJoinConfig.enabled = "True".')
param systemAssignedIdentity bool = false

// optional parameters
@description('Optional. The name of the virtual machine to be created. You should use a unique prefix to reduce name collisions in Active Directory. If no value is provided, a 10 character long unique string will be generated based on the Resource Group\'s name.')
param vmName string = take(toLower(uniqueString(resourceGroup().name)), 10)

// https://access.redhat.com/documentation/de-de/red_hat_openshift_local/2.5/html/release_notes_and_known_issues/minimum-system-requirements_rn-ki#linux
@description('Optional. OS image reference. In case of marketplace images, it\'s the combination of the publisher, offer, sku, version attributes. In case of custom images it\'s the resource ID of the custom image.')
param imageReference object = {
  offer: 'RHEL'
  publisher: 'RedHat'
  sku: '8-lvm-gen2'
  version: 'latest'
}

@description('Optional. Specifies the size OS disk in GB.')
param osDiskSizeGB string = '128'

@description('Optional. Specifies the storage account type for the OS disk.')
@allowed([
  'Premium_LRS'
  'PremiumV2_LRS'
  'Premium_ZRS'
  'Standard_LRS'
  'StandardSSD_LRS'
  'StandardSSD_ZRS'
  'UltraSSD_LRS'
])
param osDiskType string = 'StandardSSD_LRS'

// https://access.redhat.com/documentation/de-de/red_hat_openshift_local/2.5/html/release_notes_and_known_issues/minimum-system-requirements_rn-ki
@description('Optional. Specifies the size for the VMs.')
param vmSize string = 'Standard_E4as_v5'

@description('Optional. SSH Public Key data for Admin User.')
param adminPublicKey string = ''

@description('Optional. Resource Group name for existing SSH Public Key in Azure.')
param azurePublicKeyResourceGroupName string = resourceGroup().name

@description('Optional. Subscription ID for existing SSH Public Key in Azure.')
param azurePublicKeySubscriptionId string = subscription().subscriptionId

@description('Optional. Key name for existing SSH Public Key in Azure.')
param azurePublicKeyName string = ''

@description('Optional. Resource Group name for existing VNet in Azure.')
param vnetResourceGroupName string = resourceGroup().name

@description('Optional. Subscription ID for existing VNet in Azure.')
param vnetSubscriptionId string = subscription().subscriptionId

@description('Optional. VNet name for existing VNet in Azure.')
param vnetName string = ''

@description('Optional. Subnet name in existing VNet in Azure.')
param subnetName string = ''

@description('Optional. Specifies the priority for the virtual machine.')
@allowed([
  'Regular'
  'Low'
  'Spot'
])
param priority string = 'Regular'

@description('Optional. Specifies the eviction policy for the low priority virtual machine. Will result in \'Deallocate\' eviction policy.')
param enableEvictionPolicy bool = false

// variables
var suffix = '${app}-${environment}'

// existing SSH RSA Public Key in Azure 
resource existingAdminPublicKey 'Microsoft.Compute/sshPublicKeys@2022-11-01' existing = if(empty(adminPublicKey) && !empty(azurePublicKeyName)) {
  scope: resourceGroup(azurePublicKeySubscriptionId, azurePublicKeyResourceGroupName)
  name: azurePublicKeyName
}

// existing VNet in Azure
resource vnet 'Microsoft.Network/virtualNetworks@2022-07-01' existing = if(!empty(vnetName)) {
  scope: resourceGroup(vnetSubscriptionId, vnetResourceGroupName)
  name: vnetName
}

// existing Subnet in VNet in Azure
resource subnet 'Microsoft.Network/virtualNetworks/subnets@2022-07-01' existing = if(!empty(subnetName) && !empty(vnetName)) {
  parent: vnet
  name: subnetName
}

// virtual machine module
#disable-next-line use-resource-id-functions
#disable-next-line use-resource-symbol-reference
module virtualMachine '../../../lib/modules/Microsoft.Compute/virtualMachines/deploy.bicep' = {
  name: 'module-vm-${suffix}-${uniqueString(deployment().name, location)}'
  params: {
    name: vmName
    adminUsername: adminUserName
    disablePasswordAuthentication: true
    imageReference: imageReference
    nicConfigurations: [
      {
        deleteOption: 'Delete'
        ipConfigurations: [
          {
            name: 'ipconfig01'
            subnetResourceId: subnet.id
          }
        ]
        nicSuffix: '-nic-01'
      }
    ]
    osDisk: {
      diskSizeGB: osDiskSizeGB
      managedDisk: {
        storageAccountType: osDiskType
      }
    }
    osType: 'Linux'
    vmSize: vmSize
    publicKeys: [
      {
        keyData: empty(adminPublicKey) && !empty(azurePublicKeyName) ? existingAdminPublicKey.properties.publicKey : !empty(adminPublicKey) ? adminPublicKey : null
        path: '/home/${adminUserName}/.ssh/authorized_keys'
      }
    ]
    location: location
    systemAssignedIdentity: systemAssignedIdentity
    tags: tags
    encryptionAtHost: false
    enableAutomaticUpdates: false
    enableDefaultTelemetry: false
    priority: priority
    enableEvictionPolicy: enableEvictionPolicy
  }
}

