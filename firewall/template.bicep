// ,------.,--.                                 ,--.,--. 
// |  .---'`--',--.--. ,---. ,--.   ,--. ,--,--.|  ||  | 
// |  `--, ,--.|  .--'| .-. :|  |.'.|  |' ,-.  ||  ||  | 
// |  |`   |  ||  |   \   --.|   .'.   |\ '-'  ||  ||  | 
// `--'    `--'`--'    `----''--'   '--' `--`--'`--'`--' 
// Azure Firewall

// parameters-common.json
param tags object
param app string
param environment string

// module parameters (parameters-template.json)
@description('Required. VNet name for existing VNet in Azure.')
param vnetName string

// optional parameters
@description('Optional. Location for all resources.')
param location string = resourceGroup().location

@description('Optional. Resource Group name for existing VNet in Azure.')
param vnetResourceGroupName string = resourceGroup().name

@description('Optional. Subscription ID for existing VNet in Azure.')
param vnetSubscriptionId string = subscription().subscriptionId

// variables
var suffix = '${app}-${environment}'

// existing VNet in Azure
resource vnet 'Microsoft.Network/virtualNetworks@2022-07-01' existing = if(!empty(vnetName)) {
  scope: resourceGroup(vnetSubscriptionId, vnetResourceGroupName)
  name: vnetName
}

module azureFirewalls '../lib/modules/Microsoft.Network/azureFirewalls/deploy.bicep' = {
  name: 'module-afw-${suffix}-${uniqueString(deployment().name, location)}'
  params: {
    location: location
    tags: tags
    name: 'afw-${environment}'
    enableDefaultTelemetry: false
    vNetId: vnet.id
  }
}
