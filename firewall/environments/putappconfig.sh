#!/usr/bin/env bash
# putappconfig.sh
# Script to put environment configuration to key vault base64 encoded secrets.
# Updates AppConfig references to key vault secrets for keeping track of the items.
#
# What it does:
# ./environments/ENVIRONMENT.env 
#    - store in key vault
#       - use base64 encoding
#    - make a reference in appconfig 
#       - use environments/ENVIRONMENT.env as key
#       - use ENVIRONMENT as label
# ./environments/ENVIRONMENT/*.json
#    - store in key vault
#       - use base64 encoding
#    - make a reference in appconfig 
#       - use environments/ENVIRONMENT.env as key
#       - use ENVIRONMENT as label
#
# Example:
# ENVIRONMENT=dev LABEL=myorg KEYVAULT=mykeyvault APPCONFIG=myappconfig ./putappconfig.sh

set -e

# use default environment unless environment variable is set
: "${ENVIRONMENT:=dev}"

# load environment variables from environment dotenv if available
if test -f "${ENVIRONMENT}.config.env"; then
  echo -e "> Loading environment variables from: \t./${ENVIRONMENT}.config.env"
  set -o allexport
  source "${ENVIRONMENT}.config.env"
  set +o allexport
fi

: "${APP:=${1}}"
: "${KEYVAULT:=${2}}"
: "${APPCONFIG:=${3}}"
: "${LABEL:=${ENVIRONMENT}}"

# define key namespace version (used as: <key>@version/...)
: "${NS_VERSION:=27-03-2023}"

if test -z "${APP}"; then
    echo "ERROR: App name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name]"
    echo -e "\tapp name = can be set using APP environment variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT environment variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG environment variable or as command line parameter"
    exit 1
fi

if test -z "${KEYVAULT}"; then
    echo "ERROR: Key Vault name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name]"
    echo -e "\tapp name = can be set using APP environment variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT environment variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG environment variable or as command line parameter"
    exit 1
fi

if test -z "${APPCONFIG}"; then
    echo "ERROR: App Config store name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name]"
    echo -e "\tapp name = can be set using APP environment variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT environment variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG environment variable or as command line parameter"
    exit 1
fi

echo -e "Syncing environment configuration to ${KEYVAULT} vault and ${APPCONFIG} store...\n"
echo -e "  App name is: \t\t\t${APP}"
echo -e "  Environment dotenv is: \t${ENVIRONMENT}.env"
echo -e "  Parameters path is: \t\t./${ENVIRONMENT}"
echo -e "  Encoding content in: \t\tbase64"
echo -e "  AppConfig references in: \t${APPCONFIG}"
echo -e "  AppConfig label is: \t\t${LABEL}"
echo -e "  AppConfig ns version is: \t@${NS_VERSION}"

sleep 4

if test -z "${ENVIRONMENT}"; then
    echo "ERROR: ENVIRONMENT not set!"
    exit 1
fi

if ! test -f "./${ENVIRONMENT}.env"; then
    echo "ERROR: ./${ENVIRONMENT}.env not found!"
    exit 1
fi

if ! test -d "./${ENVIRONMENT}"; then
    echo "ERROR: Path ./${ENVIRONMENT} not found or not directory!"
    exit 1
fi

DOTENV_SECRET_NAME="${APP}-environments-${ENVIRONMENT}-dotenv"
DOTENV_SECRET="$(echo ${DOTENV_SECRET_NAME} | sha256sum | awk '{print $1}' | base64 -w 0)"
DOTENV_SECRET="${DOTENV_SECRET%?*}"
echo -e "\n> Syncing ${ENVIRONMENT}.env to vault ${DOTENV_SECRET}..."

az keyvault secret set \
    --vault-name "${KEYVAULT}" \
    --name "${DOTENV_SECRET}" \
    --encoding base64 \
    --file "./${ENVIRONMENT}.env" >/dev/null

DOTENV_KEY="${APP}/environments@${NS_VERSION}/${ENVIRONMENT}.env"
echo -e "> Updating reference in appconfig ${APPCONFIG}/${DOTENV_KEY}..."
az appconfig kv set-keyvault \
    --name "${APPCONFIG}" \
    --key "${DOTENV_KEY}" \
    --label "${LABEL}" \
    --secret-identifier "https://${KEYVAULT}.vault.azure.net/secrets/${DOTENV_SECRET}" \
    --yes >/dev/null

echo -e "\n  Scanning environment configuration in ./$ENVIRONMENT/*.json" 
for paramfile in ./$ENVIRONMENT/*.json; do
    if ! test -f $paramfile; then
        echo -e "# ERROR: Problem reading $paramfile, skipping..."
        continue
    fi
    file=$(basename ${paramfile})
    name=$(basename ${paramfile} .json)
    PARAM_SECRET_NAME="${APP}-environment-${ENVIRONMENT}-param-${name}"
    PARAM_SECRET="$(echo ${PARAM_SECRET_NAME} | sha256sum | awk '{print $1}' | base64 -w 0)"
    PARAM_SECRET="${PARAM_SECRET%?*}"
    echo "> Syncing ${file} to vault ${KEYVAULT}/${PARAM_SECRET}..."
    az keyvault secret set --vault-name "${KEYVAULT}" --name "${PARAM_SECRET}" --encoding base64 -f "./${ENVIRONMENT}/${file}" >/dev/null
    PARAM_KEY="${APP}/environments@${NS_VERSION}/${ENVIRONMENT}/${name}.json"
    echo -e "> Updating reference in appconfig ${APPCONFIG}/${PARAM_KEY}..."
    az appconfig kv set-keyvault \
        --name "${APPCONFIG}" \
        --key "${PARAM_KEY}" \
        --label "${LABEL}" \
        --secret-identifier "https://${KEYVAULT}.vault.azure.net/secrets/${PARAM_SECRET}" \
        --yes >/dev/null

done