#!/usr/bin/env bash
# getappconfig.sh
# Script to get App Config from Azure to current folder.
#
# Example:
# ENVIRONMENT=dev LABEL=myorg KEYVAULT=mykeyvault APPCONFIG=myappconfig ./getappconfig.sh

# use default environment unless environment variable is set
: "${ENVIRONMENT:=dev}"

# load environment variables from environment dotenv if available
if test -f "${ENVIRONMENT}.config.env"; then
  echo -e "> Loading environment variables from: \t./${ENVIRONMENT}.config.env"
  set -o allexport
  source "${ENVIRONMENT}.config.env"
  set +o allexport
fi

: "${APP:=${1}}"
: "${KEYVAULT:=${2}}"
: "${APPCONFIG:=${3}}"
: "${LABEL:=${4}}"

if test -z "${APP}"; then
    echo "ERROR: App name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name] [label name]"
    echo -e "\tapp name = can be set using APP environment variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT environment variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG environment variable or as command line parameter"
    echo -e "\tapp config label = can be set using LABEL environment variable or as command line parameter"
    exit 1
fi

if test -z "${KEYVAULT}"; then
    echo "ERROR: Key Vault name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name] [label name]"
    echo -e "\tapp name = can be set using APP environment variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT environment variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG environment variable or as command line parameter"
    echo -e "\tapp config label = can be set using LABEL environment variable or as command line parameter"
    exit 1
fi

if test -z "${APPCONFIG}"; then
    echo "ERROR: App Config store name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name] [label name]"
    echo -e "\tapp name = can be set using APP environment variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT environment variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG environment variable or as command line parameter"
    echo -e "\tapp config label = can be set using LABEL environment variable or as command line parameter"
    exit 1
fi

if test -z "${LABEL}"; then
    echo "ERROR: App Config label name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name] [label name]"
    echo -e "\tapp name = can be set using APP environment variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT environment variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG environment variable or as command line parameter"
    echo -e "\tapp config label = can be set using LABEL environment variable or as command line parameter"
    exit 1
fi

echo -e "Downloading environment configuration from ${APPCONFIG} store and ${KEYVAULT} vault...\n"
echo -e "  App name is: \t\t\t${APP}"
echo -e "  Decoding content from: \tbase64"
echo -e "  AppConfig references in: \t${APPCONFIG}"
echo -e "  AppConfig label is: \t\t${LABEL}"
echo -e "  AppConfig ns version is: \t@${NS_VERSION}"
echo -e "\n  ** TARGET **"
echo -e "  Environment dotenv is: \t${ENVIRONMENT}.env"
echo -e "  Parameters path is: \t\t./${ENVIRONMENT}"

if test -z "${ENVIRONMENT}"; then
    echo "ERROR: ENVIRONMENT not set!"
    exit 1
fi

sleep 4

DOTENV_KEY="${APP}/environments@${NS_VERSION}/${ENVIRONMENT}.env"
echo "Looking up (label:key): ${LABEL}:${DOTENV_KEY}..."
dotenv=$(az appconfig kv list -n ${APPCONFIG} --label "${LABEL}" --key "${DOTENV_KEY}" --resolve-keyvault --query "[*].value" -o tsv)
if ! test -z "${dotenv}"; then
    echo $dotenv | tr -d ' ' | base64 -d
    echo -e ""
    fileout="${ENVIRONMENT}.env"
    echo "Writing ${fileout}..."
    backupfailed=0
    if test -f "${fileout}"; then
        backupfile="${fileout}.$(date +%F_%H_%M_%S)"
        cp "${fileout}" "${backupfile}"
        if test "${?}" -ne 0; then
            echo "WARN: Error while copying ${fileout} to ${backupfile}"
            backupfailed=1
        fi
        echo "Previous file saved as ${backupfile}." 
    fi
    if test $backupfailed == 0; then
        echo $dotenv | tr -d ' ' | base64 -d > "${fileout}"
        echo -e "" >> "${fileout}"
    else
        echo " - skipping ${fileout} !"
    fi
else
    echo "WARN: Environment file not found from App Config."
fi

KEYS_BASE="${APP}/environments@${NS_VERSION}/${ENVIRONMENT}/*"
echo "Looking up environment keys..."
keys=$(az appconfig kv list -n ${APPCONFIG} --label "${LABEL}" --key "${KEYS_BASE}"  --resolve-keyvault --query "[*].key" -o tsv)

for key in $keys; do
    echo "Loading key \"$key\"..."
    value=$(az appconfig kv list -n ${APPCONFIG} --key "$key" --resolve-keyvault --query "[*].value" -o tsv)
    if ! test -z "${value}"; then
        echo $value | tr -d ' ' | base64 -d
        echo -e ""
        file=$(basename ${key})
        name=$(basename ${key} .json)
        fileout="${ENVIRONMENT}/${name}.json"
        echo "Writing ${fileout}..."
        backupfailed=0
        if test -f "${fileout}"; then
            backupfile="${fileout}.$(date +%F_%H_%M_%S)"
            cp "${fileout}" "${backupfile}"
            if test "${?}" -ne 0; then
                echo "WARN: Error while copying ${fileout} to ${backupfile}"
            fi
            echo "Previous file saved as ${backupfile}." 
        fi
        if test $backupfailed == 0; then
            echo $value | tr -d ' ' | base64 -d > "${fileout}"
            echo -e "" >> "${fileout}"
        else
            echo " - skipping ${fileout} !"
        fi
    fi
done