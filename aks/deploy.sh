#!/usr/bin/env bash
#      ,--.              ,--.                        ,--.      
#    ,-|  | ,---.  ,---. |  | ,---.,--. ,--.   ,---. |  ,---.  
#   ' .-. || .-. :| .-. ||  || .-. |\  '  /   (  .-' |  .-.  | 
#   \ `-' |\   --.| '-' '|  |' '-' ' \   '.--..-'  `)|  | |  | 
#    `---'  `----'|  |-' `--' `---'.-'  / '--'`----' `--' `--' 
#                 `--'             `---'                       
#
# Script to deploy Bicep template using az cli.
# Use optional parameter --validate to validate the deployment instead of creating the deployment.
#
# Usage: deploy.sh -i <instance> [-d] [-y] [-h]
#
# Environment variables should be in  ./instances/[INSTANCE].env
# Template parameters should be in    ./instances/[INSTANCE]/*.json

# abort on errors
set -e

# cmdline opt: -h
function printUsage() {
  echo "usage: $(basename $0) -i <instance> [-d] [-h]"
  echo "  options:"
  echo "    -h              This help."
  echo "    -d              Optional. Dry run, only validates the template."
  echo "    -y              Optional. Deploy without confirmation."
  echo "    -i <instance>   Required. Deploy instance. Template parameters should be in"
  echo "                    ./instances/[instance]/*.json and environment variables"
  echo "                    are loaded from ./instances/[instance].env"
}

# cmdline opt: -i
function deployInstance() {
  INSTANCE=$1
  CONFIRM=$2

  # additional options to pass to az deployment group create
  AZ_OPTS=""

  TASK=create
  if ! test -z "${3}"; then
    if test "${3}" == "--validate"; then
      TASK=validate
    fi
  fi

  echo -e "> About to ${TASK} the deployment of ${INSTANCE} instance...\n"

  # load environment variables from environment dotenv if available
  if test -f "instances/${INSTANCE}.env"; then
    echo -e "  Loading instance env variables from: \t./instances/${INSTANCE}.env\n"
    set -o allexport
    source "instances/${INSTANCE}.env"
    set +o allexport
  fi

  SUBSCRIPTION=$(az account show -o tsv --query "name")
  SUBSCRIPTIONID=$(az account show -o tsv --query "id")

  # use defaults unless environment variable is set
  : "${RESOURCEGROUP:=rg-demo}"
  : "${PARAMOVERRIDE:=}"
  : "${DEPLOYMENTMODE:=Incremental}"


  echo -e "  Subscription is: \t\t\t${SUBSCRIPTION}"
  echo -e "  \t\t\t\t\t${SUBSCRIPTIONID}\n"
  echo -e "  Resource group is: \t\t\t${RESOURCEGROUP}"
  echo -e "  Deployment mode is: \t\t\t${DEPLOYMENTMODE}"
  echo -e "  Parameters are: \t\t\t./instances/${INSTANCE}/parameters-common.json"
  echo -e "  \t\t\t\t\t./instances/${INSTANCE}/parameters-template.json\n"

  sleep 2

  start_time="$(date '+%s')"

  if test -f "${PARAMOVERRIDE}"; then
    echo -e "> Loading override parameters from: \t${PARAMOVERRIDE}"
    AZ_OPTS="--parameters ${PARAMOVERRIDE}"
  fi

  if test $TASK == "create"; then
    if test $CONFIRM == true; then
      AZ_OPTS="--confirm-with-what-if $AZ_OPTS"
    else
      echo -e "> Skipping confirmation as requested (-y option)..."
    fi
  fi


  az deployment group ${TASK} \
    --name "deploy-main-${INSTANCE}" \
    --resource-group "${RESOURCEGROUP}"\
    --template-file template.bicep \
    --parameters "./instances/${INSTANCE}/parameters-common.json" \
    --parameters "./instances/${INSTANCE}/parameters-template.json" \
    --mode "${DEPLOYMENTMODE}" \
    ${AZ_OPTS}

  duration=$(($(date '+%s') - start_time))
  ds=$((duration % 60))
  dm=$(((duration / 60) % 60))
  dh=$((duration / 3600))
  TZ=UTC0 printf 'Task %s stack duration: %d:%02d:%02d\n' "${TASK}" "${dh}" "${dm}" "${ds}"
  exit 0
}

# parse command line options (-i -d -h -?)
while getopts 'yi:hd' opt; do
  case "$opt" in
    i)
      MODE=deploy
      CONFIRM=true
      INSTANCE="${OPTARG}"
      ;;
    d)
      MODE=validate
      ;;
    y)
      CONFIRM=false
      ;;
    ?|h)
      printUsage
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

# if command line option was -i
if test "${MODE}" == "deploy"; then
  deployInstance "${INSTANCE}" "${CONFIRM}"
# if command line options were -i and -d
elif test "${MODE}" == "validate"; then
  deployInstance "${INSTANCE}" "${CONFIRM}" "--validate"
fi

# deployInstance() function should exit so we arrive here with unknown syntax
printUsage
exit 1
