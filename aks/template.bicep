//   ,---.  ,--. ,--. ,---.   
//  /  O  \ |  .'   /'   .-'  
// |  .-.  ||  .   ' `.  `-.  
// |  | |  ||  |\   \.-'    | 
// `--' `--'`--' '--'`-----'  
// Azure Kubernetes Service

// parameters-common.json
param tags object

// module parameters (parameters-template.json)
@description('Required. Specifies the name of the AKS cluster.')
param name string

@description('Required. The resource id of User Assigned Identity to assign with the cluster.')
param userAssignedIdentityId string

@description('Required. Specifies the name of the resource group for AKS nodes.')
param nodeResourceGroup string

@description('Required. Properties of the primary agent pool.')
param primaryAgentPoolProfile array

// optional parameters
@description('Optional. Location for all resources.')
param location string = resourceGroup().location

@description('Optional. Define one or more secondary/additional agent pools.')
param agentPools array = []

// TODO(FIX): CARML does not yet support autoUpgradeProfile, so updates to the cluster are manual
module managedClusters '../lib/modules/Microsoft.ContainerService/managedClusters/deploy.bicep' = {
  name: 'module-aks-${uniqueString(deployment().name, location)}'
  params: {
    tags: tags
    location: location
    // Required parameters
    name: name
    primaryAgentPoolProfile: primaryAgentPoolProfile
    // Non-required parameters
    agentPools: agentPools
    aksClusterNetworkPlugin: 'kubenet'
    enableDefaultTelemetry: false
    roleAssignments: [] // best practice is to use PIM
    userAssignedIdentities: {
      '${userAssignedIdentityId}': {}
    }
    // customized, non-default settings
    aksClusterNetworkPolicy: 'calico'
    nodeResourceGroup: nodeResourceGroup
    enablePrivateCluster: true
    usePrivateDNSZone: true
    aksClusterOutboundType: 'loadBalancer'
  }
}
