                          
                                       
// ,--.   ,--.,--.   ,--. ,---.   ,---.   
//  \  `.'  / |   `.'   |'   .-' '   .-'  
//   \     /  |  |'.'|  |`.  `-. `.  `-.  
//    \   /   |  |   |  |.-'    |.-'    | 
//     `-'    `--'   `--'`-----' `-----'  
// Azure DevOps Scale Set Agent Pool

// parameters-common.json
param tags object
param app string
param environment string

// module parameters (parameters-template.json)
param adminUserName string
param scalesetName string

// optional parameters
@description('Optional. Location for all resources.')
param location string = resourceGroup().location

@description('Optional. Enables system assigned managed identity on the resource. The system-assigned managed identity will automatically be enabled if extensionAadJoinConfig.enabled = "True".')
param systemAssignedIdentity bool = true

@description('Optional. Specifies the size OS disk in GB.')
param osDiskSizeGB string = '128'

@description('Optional. Specifies the storage account type for the OS disk.')
@allowed([
  'Premium_LRS'
  'PremiumV2_LRS'
  'Premium_ZRS'
  'Standard_LRS'
  'StandardSSD_LRS'
  'StandardSSD_ZRS'
  'UltraSSD_LRS'
])
param osDiskType string = 'StandardSSD_LRS'

// https://access.redhat.com/documentation/de-de/red_hat_openshift_local/2.5/html/release_notes_and_known_issues/minimum-system-requirements_rn-ki
@description('Optional. Specifies the size for the VMs.')
param vmSize string = 'Standard_D4ads_v5'

@description('Optional. SSH Public Key data for Admin User.')
param adminPublicKey string = ''

@description('Optional. Resource Group name for existing SSH Public Key in Azure.')
param azurePublicKeyResourceGroupName string = resourceGroup().name

@description('Optional. Subscription ID for existing SSH Public Key in Azure.')
param azurePublicKeySubscriptionId string = subscription().subscriptionId

@description('Optional. Key name for existing SSH Public Key in Azure.')
param azurePublicKeyName string = ''

@description('Optional. Resource Group name for existing VNet in Azure.')
param vnetResourceGroupName string = resourceGroup().name

@description('Optional. Subscription ID for existing VNet in Azure.')
param vnetSubscriptionId string = subscription().subscriptionId

@description('Optional. VNet name for existing VNet in Azure.')
param vnetName string = ''

@description('Optional. Subnet name in existing VNet in Azure.')
param subnetName string = ''

@description('Optional. Specifies the priority for the virtual machine.')
@allowed([
  'Regular'
  'Low'
  'Spot'
])
param priority string = 'Regular'

@description('Optional. Specifies the eviction policy for the low priority virtual machine. Will result in \'Deallocate\' eviction policy.')
param enableEvictionPolicy bool = false

@description('Optional. Fault Domain count for each placement group.')
param scaleSetFaultDomain int = 2

@description('Optional. Specifies whether the Virtual Machine Scale Set should be overprovisioned.')
param overprovision bool = false

@description('Optional. Specifies the mode of an upgrade to virtual machines in the scale set.\' Manual - You control the application of updates to virtual machines in the scale set. You do this by using the manualUpgrade action. ; Automatic - All virtual machines in the scale set are automatically updated at the same time. - Automatic, Manual, Rolling.')
@allowed([
  'Manual'
  'Automatic'
  'Rolling'
])
param upgradePolicyMode string = 'Manual'

@description('Optional. Array of role assignment objects that contain the \'roleDefinitionIdOrName\' and \'principalId\' to define RBAC role assignments on this resource. In the roleDefinitionIdOrName attribute, you can provide either the display name of the role definition, or its fully qualified ID in the following format: \'/providers/Microsoft.Authorization/roleDefinitions/c2f4ef07-c644-48eb-af81-4b1b4947fb11\'.')
param roleAssignments array = []

// variables
var suffix = '${app}-${environment}'
var cloudInit = loadTextContent('cloudinit.yaml')

// existing SSH RSA Public Key in Azure 
resource existingAdminPublicKey 'Microsoft.Compute/sshPublicKeys@2022-11-01' existing = if(empty(adminPublicKey) && !empty(azurePublicKeyName)) {
  scope: resourceGroup(azurePublicKeySubscriptionId, azurePublicKeyResourceGroupName)
  name: azurePublicKeyName
}

// existing VNet in Azure
resource vnet 'Microsoft.Network/virtualNetworks@2022-07-01' existing = if(!empty(vnetName)) {
  scope: resourceGroup(vnetSubscriptionId, vnetResourceGroupName)
  name: vnetName
}

// existing Subnet in VNet in Azure
resource subnet 'Microsoft.Network/virtualNetworks/subnets@2022-07-01' existing = if(!empty(subnetName) && !empty(vnetName)) {
  parent: vnet
  name: subnetName
}

module virtualMachineScaleSets '../../lib/modules/Microsoft.Compute/virtualMachineScaleSets/deploy.bicep' = {
  name: 'module-vmss-${suffix}-${uniqueString(deployment().name, location)}'
  params: {
    tags: tags
    location: location

    customData: cloudInit

    // Required parameters
    adminUsername: adminUserName
    imageReference: {
      offer: '0001-com-ubuntu-server-focal'
      publisher: 'Canonical'
      sku: '20_04-lts-gen2'
      version: 'latest'
    }
    name: scalesetName
    osDisk: {
      diffDiskSettings: {
        option: 'Local'
        placement: 'ResourceDisk'
      }
      caching: 'ReadOnly'
      createOption: 'fromImage'
      diskSizeGB: osDiskSizeGB
      managedDisk: {
        storageAccountType: osDiskType
      }
    }
    osType: 'Linux'
    skuName: vmSize
    skuCapacity: 2
    // Non-required parameters
    nicConfigurations: [
      {
        ipConfigurations: [
          {
            name: 'ipconfig1'
            properties: {
              subnet: {
                id: subnet.id
              }
            }
          }
        ]
        nicSuffix: '-nic01'
      }
    ]
    publicKeys: [
      {
        keyData: empty(adminPublicKey) && !empty(azurePublicKeyName) ? existingAdminPublicKey.properties.publicKey : !empty(adminPublicKey) ? adminPublicKey : null
        path: '/home/${adminUserName}/.ssh/authorized_keys'
      }
    ]
    encryptionAtHost: false
    enableAutomaticUpdates: false
    enableDefaultTelemetry: false
    disablePasswordAuthentication: true
    systemAssignedIdentity: systemAssignedIdentity
    vmPriority: priority
    enableEvictionPolicy: enableEvictionPolicy
    scaleSetFaultDomain: scaleSetFaultDomain
    overprovision: overprovision
    upgradePolicyMode: upgradePolicyMode

    roleAssignments: roleAssignments

  }
}
