#!/usr/bin/env bash
# getappconfig.sh
# Script to get App Config from Azure.
#
# Queries App Configuration using labels and pre-determined key namespaces. File contents
# are stored in Key Vault and retrieved through App Configuration references to Key Vault.
#
# Expected App Configuration structure:
#
# INSTANCE ENVIRONMENT VARIABLES FILE
#       KEY = "${APP}/instances@${NS_VERSION}/${INSTANCE}.env"
#       LABEL = instance or environment custom identifier
#
# TEMPLATE PARAMETER FILES
#       KEY = "${APP}/instances@${NS_VERSION}/${INSTANCE}/*"
#       LABEL = instance or environment custom identifier
#
# Instance environment variables file will be saved as: ./<instance>.env
# Template parameter files will be saved to:            ./<instance>/
# Existing files are copied to backup before overwriting contents (<filename>.<date>).
# 
# App Configuration and Key Vault configuration can be read from environment variables or
# from optional file: <instance>.config.env
#
# Usage example:
# INSTANCE=vnet-test-5 APP=vnet LABEL=test-19 KEYVAULT=mykeyvault APPCONFIG=myappconfig ./getappconfig.sh

SUBSCRIPTION=$(az account show -o tsv --query "name")
SUBSCRIPTIONID=$(az account show -o tsv --query "id")

# use default instance unless instance variable is set
: "${INSTANCE:=dev}"

# load instance variables from instance dotenv if available
if test -f "${INSTANCE}.config.env"; then
  echo -e "> Loading instance variables from: \t./${INSTANCE}.config.env"
  set -o allexport
  source "${INSTANCE}.config.env"
  set +o allexport
fi

: "${APP:=${1}}"
: "${KEYVAULT:=${2}}"
: "${APPCONFIG:=${3}}"
: "${LABEL:=${4}}"

# define key namespace version (used as: <key>@version/...)
: "${NS_VERSION:=27-03-2023}"

if test -z "${APP}"; then
    echo "ERROR: App name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name] [label name]"
    echo -e "\tapp name = can be set using APP instance variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT instance variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG instance variable or as command line parameter"
    echo -e "\tapp config label = can be set using LABEL instance variable or as command line parameter"
    exit 1
fi

if test -z "${KEYVAULT}"; then
    echo "ERROR: Key Vault name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name] [label name]"
    echo -e "\tapp name = can be set using APP instance variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT instance variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG instance variable or as command line parameter"
    echo -e "\tapp config label = can be set using LABEL instance variable or as command line parameter"
    exit 1
fi

if test -z "${APPCONFIG}"; then
    echo "ERROR: App Config store name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name] [label name]"
    echo -e "\tapp name = can be set using APP instance variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT instance variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG instance variable or as command line parameter"
    echo -e "\tapp config label = can be set using LABEL instance variable or as command line parameter"
    exit 1
fi

if test -z "${LABEL}"; then
    echo "ERROR: App Config label name not set!"
    echo "Usage: $0 [app name] [key vault name] [app config name] [label name]"
    echo -e "\tapp name = can be set using APP instance variable or as command line parameter"
    echo -e "\tkey vault name = can be set using KEYVAULT instance variable or as command line parameter"
    echo -e "\tapp config name = can be set using APPCONFIG instance variable or as command line parameter"
    echo -e "\tapp config label = can be set using LABEL instance variable or as command line parameter"
    exit 1
fi

echo -e "Downloading instance configuration from ${APPCONFIG} store and ${KEYVAULT} vault...\n"
echo -e "  Subscription is: \t\t${SUBSCRIPTION}"
echo -e "  \t\t\t\t${SUBSCRIPTIONID}"
echo -e "  App name is: \t\t\t${APP}"
echo -e "  Decoding content from: \tbase64"
echo -e "  AppConfig references in: \t${APPCONFIG}"
echo -e "  AppConfig label is: \t\t${LABEL}"
echo -e "  AppConfig ns version is: \t@${NS_VERSION}"
echo -e "\n  ** TARGET **"
echo -e "  Instance dotenv is: \t\t${INSTANCE}.env"
echo -e "  Parameters path is: \t\t./${INSTANCE}"

if test -z "${INSTANCE}"; then
    echo "ERROR: INSTANCE not set!"
    exit 1
fi

sleep 4

DOTENV_KEY="${APP}/instances@${NS_VERSION}/${INSTANCE}.env"
echo "Looking up (label:key): ${LABEL}:${DOTENV_KEY}..."
dotenv=$(az appconfig kv list -n ${APPCONFIG} --label "${LABEL}" --key "${DOTENV_KEY}" --resolve-keyvault --query "[*].value" -o tsv)
if ! test -z "${dotenv}"; then
    echo $dotenv | tr -d ' ' | base64 -d
    echo -e ""
    fileout="${INSTANCE}.env"
    echo "Writing ${fileout}..."
    backupfailed=0
    if test -f "${fileout}"; then
        backupfile="${fileout}.$(date +%F_%H_%M_%S)"
        cp "${fileout}" "${backupfile}"
        if test "${?}" -ne 0; then
            echo "WARN: Error while copying ${fileout} to ${backupfile}"
            backupfailed=1
        fi
        echo "Previous file saved as ${backupfile}." 
    fi
    if test $backupfailed == 0; then
        echo $dotenv | tr -d ' ' | base64 -d > "${fileout}"
        echo -e "" >> "${fileout}"
    else
        echo " - skipping ${fileout} !"
    fi
else
    echo "WARN: Instance file not found from App Config."
fi

KEYS_BASE="${APP}/instances@${NS_VERSION}/${INSTANCE}/*"
echo "Looking up instance keys..."
keys=$(az appconfig kv list -n ${APPCONFIG} --label "${LABEL}" --key "${KEYS_BASE}"  --resolve-keyvault --query "[*].key" -o tsv)

for key in $keys; do
    echo "Loading key \"$key\"..."
    value=$(az appconfig kv list -n ${APPCONFIG} --key "$key" --resolve-keyvault --query "[*].value" -o tsv)
    if ! test -z "${value}"; then
        echo $value | tr -d ' ' | base64 -d
        echo -e ""
        if ! test -d "${INSTANCE}"; then
            mkdir -p "${INSTANCE}"
        fi
        file=$(basename ${key})
        name=$(basename ${key} .json)
        fileout="${INSTANCE}/${name}.json"
        echo "Writing ${fileout}..."
        backupfailed=0
        if test -f "${fileout}"; then
            backupfile="${fileout}.$(date +%F_%H_%M_%S)"
            cp "${fileout}" "${backupfile}"
            if test "${?}" -ne 0; then
                echo "WARN: Error while copying ${fileout} to ${backupfile}"
            fi
            echo "Previous file saved as ${backupfile}." 
        fi
        if test $backupfailed == 0; then
            echo $value | tr -d ' ' | base64 -d > "${fileout}"
            echo -e "" >> "${fileout}"
        else
            echo " - skipping ${fileout} !"
        fi
    fi
done